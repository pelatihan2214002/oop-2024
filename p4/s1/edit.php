<?php
$id = $_GET['id'];
require_once 'database.php';
$query = "SELECT * FROM data_pengguna WHERE id=$id";
$sql = mysqli_query($koneksi_database, $query);
$data = mysqli_fetch_all($sql, MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel Pengguna</title>
    <link rel="stylesheet" href="http://localhost/css/bootstrap.min.css" />
</head>
<body class="container">
<h1>Form Edit Pengguna</h1>
<form method="post" action="update.php?id=<?=$data[0]["id"] ?>">
<div class="mb-3">
  <label for="nama_pengguna" class="form-label">Nama Pengguna</label>
  <input type="text" class="form-control" id="nama_pengguna" name="nama_pengguna" value="<?=$data[0]["nama_pengguna"] ?>" >
</div>
<div class="mb-3">
  <label for="email_pengguna" class="form-label">Email Pengguna</label>
  <input type="email" class="form-control" id="email_pengguna" name="email_pengguna" value="<?=$data[0]["email_pengguna"] ?>" >
</div>
<div class="mb-3">
  <label for="password_pengguna" class="form-label">Password Pengguna</label>
  <input type="password" class="form-control" id="password_pengguna" name="password_pengguna" value="<?=$data[0]["password_pengguna"] ?>" >
</div>
<button type="submit" class="btn btn-success">Simpan</button>
</form>
</body>
</html>