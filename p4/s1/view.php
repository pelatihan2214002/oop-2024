<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel Pengguna</title>
    <link rel="stylesheet" href="http://localhost/css/bootstrap.min.css" />
</head>
<body class="container">
  <div class="d-flex flex-row bg-dark justify-content-between align-self-center p-4">
      <h1 class="text-light">Tabel Pengguna</h1>
    <div>
    <a  type="button" class="btn btn-primary " href="tambah.php">Tambah</a>
    </div>
    </div>
    <div>
    <table class="table table-stripped table-hover mt-4" >
  <thead class="table-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Pengguna</th>
      <th scope="col">Email Pengguna</th>
      <th scope="col">Password Pengguna</th>
      <th scope="col">Opsi</th>
    </tr>
  </thead>
  <tbody>
    <?php
for ($step_awal = 0; $step_awal < $total_data; $step_awal = $step_awal + 1) {
    echo "<tr>";
    echo "<td>" . ($step_awal + 1) . "</td>";
    echo "<td>" . $data[$step_awal]["nama_pengguna"] . "</td>";
    echo "<td>" . $data[$step_awal]["email_pengguna"] . "</td>";
    echo "<td>" . $data[$step_awal]["password_pengguna"] . "</td>";
    echo "<td>"; // td pembuka
    echo '<a href="http://localhost/pelatihan/p4/s1/edit.php?id='. $data[$step_awal]['id'] . ' " class="btn btn-sm btn-warning">Edit</a>';
    echo '<a href="http://localhost/pelatihan/p4/s1/hapus.php?id='. $data[$step_awal]['id'] . ' " class="btn btn-sm btn-danger">Hapus</a>';
    echo "</td>"; // td penutup
    echo "</tr>";
}
?>
  </tbody>
</table>
    </div>
</body>
</html>