<?php

include_once 'model.php';
class Controller
{
    public function invoke()
    {
        $model_data = new Model();
        $row_data = $model_data->getData();
        $data = '<table border=1> <tr><th>No</th><th>Judul</th><th>Pengarang</th> <th>Penerbit</th><th>Tahun</th></tr>';
        foreach ($row_data as $key => $value) {
            $data .= '<tr>' . '<td>' . ($key + 1) . '</td>' . '<td> ' . $value->judul . ' </td>' . '<td>' . $value->pengarang . '</td>' . '<td>' . $value->penerbit . '</td>' . '<td>' . $value->tahun . '</td>' . '</tr>';
        }
        include 'view.php';
    }
}