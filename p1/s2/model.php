<?php
include_once 'book.php';
class Model
{
    public function getData()
    {
        return array(
            new Book("PHP", 'sandika', 'CV PRINTER', 2022),
            new Book("JS", 'SURIAH', 'CV PRINTER', 2105),
            new Book("CSS", 'MADRIAH', 'CV PRINTER', 2102),
            new Book("HTML", 'ROSIDI', 'CV PRINTER', 2001),
            new Book("PHP", 'PAARI', 'CV PRINTER', 2003),
        );
    }
}