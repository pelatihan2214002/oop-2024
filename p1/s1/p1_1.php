<?php
class Hewan
{
    protected $jml_kaki = 10;
    public $warna_kulit = "cokelat";

    public function berpindah()
    {
        echo "berpindah";
    }
    final public function makan()
    {
        echo "makan <br>";
    }
}

class Kucing extends Hewan
{
    public function berpindah()
    {
        parent::berpindah();
        echo $this->warna_kulit;
        echo $this->jml_kaki;
        echo "kucing berpindah";
    }
}

class Burung extends Hewan
{
    protected $sayap;
    public function berpindah()
    {
        parent::makan();
        echo "burung berpindah";
    }
}

$hewan = new Hewan();
$hewan->berpindah();
echo "<br>";
$kucing = new Kucing();
$kucing->berpindah();
echo "<br>";
$burung = new Burung();
$burung->berpindah();