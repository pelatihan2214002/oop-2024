<?php

class Manusia
{
    protected $nama;
    private $usia;

    function __construct($nama = "Samsul", $usia = 20)
    {
        if (!is_int($usia)) {
            throw new Exception("Usia harus angka");
        }
        $this->nama = $nama;
        $this->usia = $usia;
        $this->sayHello();
    }
    function sayHello()
    {
        $hello = "Hello my name is " . $this->nama . " and I am " . $this->usia . " years old";
        echo $hello;
    }
}

$samsul = new Manusia('Aldin', 19);