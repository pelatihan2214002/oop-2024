<?php

abstract class Buah
{
    abstract public function makan();
    abstract public function minum();
}

class Manggis extends Buah
{
    public function makan()
    {
        echo "makan manggis";
    }
    public function minum()
    {
        echo "minu jus manggis";
    }
}

$manggis = new Manggis;
$manggis->makan();
echo "<br>";
$manggis->minum();