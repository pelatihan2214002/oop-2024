<?php

function hitung($bil1, $bil2, $operator)
{
    $hasil = 0;
    if ($operator == "+") {
        $hasil = $bil1 + $bil2;
        cetak($bil1, $bil2, $operator, $hasil);
    } else if ($operator == '-') {
        $hasil = $bil1 - $bil2;
        cetak($bil1, $bil2, $operator, $hasil);
    } else if ($operator == '*') {
        $hasil = $bil1 * $bil2;
        cetak($bil1, $bil2, $operator, $hasil);
    } else if ($operator == '/') {
        $hasil = $bil1 / $bil2;
        cetak($bil1, $bil2, $operator, $hasil);
    } else {
        echo "<p>Maaf, operator yang anda pilih tidak tersedia</p>";
    }
    return $hasil;
}

function cetak($bil1, $bil2, $operator, $hasil)
{
    echo "Hasil : " . $bil1 . " " . $operator . " " . $bil2 . " = " . $hasil . "<br>";
}