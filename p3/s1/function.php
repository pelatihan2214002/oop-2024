<?php

// fungsi return = mengembalikan sebuah nilai
// fungsi void = tidak mengembalikan sebuah nilai

function tambah($bil1, $bil2)
{
    $hasil = $bil1 + $bil2;
    return $hasil;
}

function cetak($cetak)
{
    echo 'hasil dari penjumlahan itu adalah ' . $cetak . '<br/>';
}

for ($i = 1; $i <= 10; $i++) {
    $tampung = tambah($i, $i);
    cetak($tampung);
}